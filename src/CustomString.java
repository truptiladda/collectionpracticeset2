
public final class CustomString {

	char[] string = null;

	CustomString(char[] str) {
		this.string = str;

	}

	int length() {
		return string.length;

	}

	char charAt(int i) {
		return string[i];

	}

	void printString(CustomString str) {
		for (int i = 0; i < str.string.length; i++) {
			System.out.print(str.string[i]);
		}

	}

	char[] toCharArray(CustomString str) {
		return str.string;

	}

	CustomString concat(CustomString s1, CustomString s2) {
		char[] result = new char[s1.length() + s2.length()];
		char[] a1 = s1.toCharArray(s1);
		char[] a2 = s1.toCharArray(s2);
		for (int i = 0; i < s1.length(); i++) {
			result[i] = a1[i];

		}
		for (int i = s1.length(), j = 0; i < result.length; i++, j++) {

			result[i] = a2[j];

		}

		return new CustomString(result);

	}

	public static void main(String[] args) {
		char[] str = { 'a', 'b', 'c', 'd', 'e' };
		CustomString string = new CustomString(str);
		System.out.println(string.charAt(4));
		System.out.println(string.length());

		string.printString(string.concat(string, string));

	}

}
